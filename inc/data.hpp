#include <fstream>
#include <string>
#include <vector>
#include <queue>
#include <algorithm>
#include <iomanip>
#include "task.hpp"
#include <limits>

typedef std::priority_queue<Task,std::vector<Task>,TaskOrder> myQueue;

class Data {
  std::fstream infile,outfile;
  
  myQueue * N;
  myQueue * G;
  
  std::vector<Task> pi; // permutacja - zawiera kolejność
  std::vector<Task> piopt;
  int Cmax;
  int UB; 
  int glebia; 
  
  int size;
  //int calculated;
public:
  
  Data() {
    size = 0;
    N = new myQueue(false);
    G = new myQueue(true);
  }

  Data(std::string infilename,std::string outfilename) {
    size = 0;
    Cmax = 0;
    UB = 99999999;
    glebia=0;
    N = new myQueue(false);
    G = new myQueue(true);
    openInFile(infilename);
    openOutFile(outfilename);
  }
	
  ~Data() {
    infile.close();
    outfile.close();
    delete N;
    delete G;
  }
  
  bool openInFile(std::string infilename) {
    infile.open(infilename.c_str(),std::ios::in);
    if (!infile.is_open()) {
      std::cerr << "Brak pliku wejściowego." << std::endl;
      return false;
    }
    else {
      return true;
    }
  }
	
	bool openOutFile(std::string outfilename) {
		outfile.open(outfilename.c_str(),std::ios::out);
		if (!outfile.is_open()) {
			std::cerr << "Brak pliku wyjściowego." << std::endl;
			return false;
		} else {
			return true;
		}
	}
	
	void clear() {
		
	}
	
	int read() {
		
		Task task;
		
		clear();
		
		/* inicjalizacja */
		
		infile >> size;
		
		for (int i = 1; i <= size; i++) {
			int r,p,q;
			infile >> r;
			infile >> p;
			infile >> q;
			task.setIRPQ(i,r,p,q);
			N->push(task);
		}
		
		
		
		return size;
	}

	void write() {
		
	}
	
	/**
		\brief wyświetla zawartość wszystkich tablic w kolejności wg permutacji pi
	*/
	void printN() {
		myQueue N = *(this->N);
		//	std::cout << "  r   |   p   |   q   |   s   |   c   |" << std::endl;
		//	std::cout << "======|=======|=======|=======|=======|" << std::endl;
		while (!N.empty()) {
		  std::cout << N.top().getI() << " ";
			N.pop();
		}
		std::cout << std::endl;
	}
	
	void printPi() {
	  //std::cout << "  r   |   p   |   q   |   s   |   c   |" << std::endl;
	  //std::cout << "======|=======|=======|=======|=======|" << std::endl;
	  for (int i = 0; i <(int) pi.size(); i++) {
		  std::cout << pi[i].getI() << " ";
		}
		std::cout << std::endl;
	}

  public:
  std::vector<Task> S() {
    myQueue kopia = *(this->N);
    myQueue * N=&kopia;
    std::vector<Task> pi;
    	  myQueue * G = new myQueue(true);
		int t = N->top().getR();
		int k = 0;
		Cmax=0;
		//std::cout << t << std::endl;
		Task e;
		while (!N->empty() || !G->empty()) {
			//std::cout << "Ready (t = " << t << "): "<< std::endl;
			while (!N->empty() && (N->top().getR() <= t)) {
				e = N->top();
				G->push(e);
				//std::cout << e << std::endl;
				N->pop();
			}
			
			if (G->empty()) {
				t = N->top().getR();
			} else {
				e = G->top();
				G->pop();
				k++;
				//e.setS(std::max(t,pi[k-1].getS()+pi[k-1].getP()));
				e.setS(t);
				e.setC(t+e.getP());
				
				pi.push_back(e);
				t = t + e.getP();
				Cmax = std::max(Cmax,t+e.getQ());
			}
			
		}
		std::cout << Cmax << std::endl;
		return pi;
	}
	
  void prmtS() {
    myQueue kopia = *(this->N);
    myQueue * N=&kopia;
     std::vector<Task> pi;
	  myQueue * G = new myQueue(true);
		int t = N->top().getR();
		int k = 0;	
		Task e;
		Task l;
		Cmax=0;
		while (!N->empty() || !G->empty()) {
			//std::cout << "Ready (t = " << t << "): "<< std::endl;
			while (!N->empty() && (N->top().getR() <= t)) {
				e = N->top();
				G->push(e);
				N->pop();
				/* podział */
				if (k > 0) {
					if (e.getQ() > l.getQ()) {
					  pi.pop_back();
					  l.setP(t-e.getR());
					  t = e.getR();
					  if (l.getP() > 0) {
					    G->push(l);
						}
					}
				}
			}
			
			
			
			
			if (G->empty()) {
				t = N->top().getR();
			} else {
				e = G->top();
				G->pop();
				l=e;
				k++; 
				pi.push_back(e);
				t = t + e.getP();
				Cmax = std::max(Cmax,t+e.getQ());
			}
			
		}
		std::cout <<"PRE " << Cmax << std::endl;
		//return pi;
  }


	
  int Calier()
  {
    int a=0,b=0;
    int c = -1;
    int LB=0, U=0;
	std::vector<Task> pi;
	pi = S();
    //printPi();
    U=Cmax;
    if(U<UB)
      {
      UB=U; 
      piopt=pi;
      }
    for (int i=0;i<(int)pi.size();i++) 
      {
      if (pi[i].getC()+pi[i].getQ() == Cmax)
	{
	b = i;
	}
      }		
    for (int i = b; i >= 0; i--) 
      {
	if (pi[i].getC() < pi[i+1].getS())
	  {
	    a = i+1;
	    break;
	  }
      }
    for (int i = a; i < b; i++)
      {
      if (pi[i].getQ() < pi[b].getQ())
	{
	  c = i;

	}
      }
    std::cout << (a+1) << " " << (b+1) << " " << (c+1) << " " << glebia << std::endl;
    if (c < 0)
      {
	return UB;
      }
    else
      {
	int minR=999999;
	int minQ=999999;
	int sumP = 0;
	int old;
	for (int i=c+1; i<=b; i++)
	  {
	    minR = std::min(minR, pi[i].getR());
	    minQ = std::min(minQ, pi[i].getQ());
	    sumP+=pi[i].getP();
	  }
	old=pi[c].getR();
	pi[c].setR(std::max(pi[c].getR(), minR+sumP));
	while(!N->empty())
	  {
	    N->pop();
	  }
	for (int i = 0; i <(int) pi.size(); i++)
	  {
	    N->push(pi[i]);
	  }
	prmtS(); 
	LB=Cmax;
	if(LB<UB)
	  {
	    glebia++;
	    U=Calier();
	    glebia--;
	  }
	pi[c].setR(old);
	old=pi[c].getQ();
	pi[c].setQ(std::max(pi[c].getQ(), minQ+sumP));
	while(!N->empty())
	  {
	    N->pop();
	  }
	for (int i = 0; i <(int) pi.size(); i++)
	{
		N->push(pi[i]);
	}
	prmtS();
	LB=Cmax;
	if(LB<UB)
	  {
	    glebia++;
	    U=Calier();
	    glebia--;
	  }
	pi[c].setQ(old);
	return UB;
      }
    
  }
	
	int getSize() {
		return size;
	}
	
};
