#include <iostream>

class Task {
	friend class TaskOrder;
	friend std::ostream& operator<<(std::ostream&, const Task&);
  int i,r,p,q,s,c;
public:
	Task() {
	  this->i=0;
		this->r = 0;
		this->p = 0;
		this->q = 0;
		this->s = 0;
		this->c = 0;
	}
	
  Task(int i, int r, int p, int q) {
    setIRPQ(i,r,p,q);
	}
	
  void setIRPQ(int i, int r, int p, int q) {
    this->i = i;
		this->r = r;
		this->p = p;
		this->q = q;
	}
	
  int getI() const {
    return i;
  }
	int getR() const {
		return r;
	}
	
	int getP() const {
		return p;
	}
	
	int getQ() const {
		return q;
	}
	
	int getS() const {
		return s;
	}
	
	int getC() const {
		return c;
	}
	
	void setP(int p) {
		this->p = p;
	}
	
	void setS(int s) {
		this->s = s;
	}
	
	void setC(int c) {
		this->c = c;
	}

  void setQ(int q) {
    this->q = q;
  }

  void setR(int r) {
    this->r = r;
  }
};

class TaskOrder {
	bool byQ;
public:	
	TaskOrder(bool byQ) {
		this->byQ = byQ;
	}
	
	bool operator()(const Task & a, const Task & b) {
		if (byQ) {
			return a.q < b.q;
		} else {
			return a.r > b.r;	
		}
		
	}
};

std::ostream& operator<<(std::ostream& os, const Task& task) {
	os << std::setw(5) << task.r << " | " << std::setw(5) << task.p <<" | "<< std::setw(5)<< task.q << " | " << std::setw(5) << task.s << " | " << std::setw(5) << task.c << " | " << std::endl;
	return os;
}
